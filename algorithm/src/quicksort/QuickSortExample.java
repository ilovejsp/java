package quicksort;

public class QuickSortExample {
	static int arr[]={33311,2,3331,21,2999,10000,234,1};
	private static int partition(int left,int right){
		int i=left;
		int j=right-1;
		int pivot=arr[right];
		
		for(;;){
			while(arr[i]<pivot){
				i++;
			}
			while(i<j && arr[j]>pivot){
				j--;
			}
			if(i>=j){
				break;
			}else{
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}
		int temp=arr[i];
		arr[i]=arr[right];
		arr[right]=temp;
		return i;
	}
	private static void quickSort(int left,int right){
		if(left>=right){
			return;
		}
		int pivot=partition(left, right);
		quickSort(left,pivot-1);
		quickSort(pivot,right);
		
	}
	private static void sort(){
		quickSort(0, arr.length-1);
	}
	
	public static void main(String[] args){
		for(int i:arr){
			System.out.println(i);
		}
		System.out.println("-------");
		sort();
		for(int i:arr){
			System.out.println(i);
		}
	}

}
