package sort;

public class SelectionSort {
	public static int[] sort(int[] arr){
		int n=arr.length;
		for(int i=0; i<n-1;i++){
			int lowest=i;
			int lowkey=arr[i];
			for(int j=i+1; j<n; j++){
				if(arr[j]<lowkey){
					lowest=j;
					lowkey=arr[j];
				}
			}
			int temp=arr[i];
			arr[i]=arr[lowest];
			arr[lowest]=temp;
		}
		return arr;
	}

	public static void main(String[] args) {
		int arr[]={333,111,22,10000,3,111111,23512,31};
		arr=sort(arr);
		for(int i:arr){
			System.out.println(i);
		}
	}

}
