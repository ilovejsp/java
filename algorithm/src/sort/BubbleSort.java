package sort;

public class BubbleSort {
	public static int[] sort(int[] a){
		int n=a.length;
		
		for(int i=0; i<n;i++){
			for(int j=0; j<n-1;j++){
				if(a[j]>a[j+1]){
					int temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
				}
			}
		}
		return a;
	}

	public static void main(String[] args) {
		int array[]={3,4,11,22,90,1001,2};
		array=sort(array);
		for(int i : array){
			System.out.println(i);
		}
	}

}
