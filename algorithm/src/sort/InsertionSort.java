package sort;

public class InsertionSort {
	public static int[] sort(int[] arr){
		int n=arr.length;
		
		for(int i=1; i<n; i++){
			int j=i;
			while(j>=1 && arr[j-1]>arr[j]){
				int temp=arr[j-1];
				arr[j-1]=arr[j];
				arr[j]=temp;
				j--;
			}
		}
		
		return arr;
	}

	public static void main(String[] args) {
		int arr[]={333,111,22,10000,3,111111,23512,31};
		arr=sort(arr);
		for(int i:arr){
			System.out.println(i);
		}

	}

}
