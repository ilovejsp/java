package queue;
/**
 * 
 * @author ilovejsps
 * 배열로 구현한 큐
 */

class MyQueue {
	Object[] queue;	//큐
	int queueSize;	//큐크기
	int front;
	int rear;
	
	//큐의 기본 크기
	static int DEFAULT_QUEUE_SIZE=100;
	
	public MyQueue(){
		this(DEFAULT_QUEUE_SIZE);
	}
	public MyQueue(int size){
		queueSize=size;
		queue=new Object[size];
		front=rear=0;
	}
	/**
	 * 에러 처리
	 * 메시지 s를 표시하고 프로그램을 종료시킨다.
	 */
	private void error(String s){
		System.err.println(s);
		System.exit(1);
	}
	private int next(int a){
		return (a+1) % queueSize;
	}
	/**
	 * 큐를 비운다.
	 */
	public void clear(){
		front=rear=0;
	}
	
	/**
	 * 큐에 데이터를 넣는다.
	 */
	public void enqueue(Object x){
		if(next(rear) == front){
			error("이 이상 큐에 요소를 추가할수 없습니다.");
		}
		queue[rear]=x;
		rear=next(rear);
	}
	/**
	 * 큐에서 데이터를 꺼낸다
	 */
	public Object dequeue(){
		if(front==rear){
			error("큐가 비어있기 때문에 요소를 꺼낼수 없습니다.");
		}
		Object x = queue[front];
		front=next(front);
		return x;
	}
	public boolean isEmpty(){
		return front==rear;
	}
	public String toString(){
		String s;
		s="MyQueue=[";
		for(int i=front; i!=rear; i=next(i)){
			s+=queue[i]+" ";
		}
		s+="] front=" + front + ",rear="+rear;
		return s;
	}
	public static void main(String[] args){
		MyQueue q = new MyQueue(5);
		q.enqueue("a");
		q.enqueue("b");
		q.enqueue("c");
		System.out.println(q);
		q.dequeue();
		System.out.println(q);
		q.dequeue();
		q.enqueue("d");
		System.out.println(q);
	}
}
