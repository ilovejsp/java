package algorithmbook;

public class LeeHangSu {
	int leeCalc(int a,int b){
		return fac(a)/fac(b)*fac(a-b);
	}
	int fac(int n){
		if(n==1){
			return 1;
		}else{
			return n*fac(n-1);
		}
	}
	public static void main(String[] args) {
		LeeHangSu lhs = new LeeHangSu();
		System.out.println(lhs.leeCalc(3,2));
	}

}
