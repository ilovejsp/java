package algorithmbook;

import java.util.Scanner;

public class LcmGcd {
	//최대공약수
	int gcd(int a,int b){
		int ret=0;
		if(a>=b){
			if(a%b==0){
				ret=b;
			}else{
				return gcd(b,a%b);
			}
		}else{
			return gcd(b,a);
		}
		return b;
	}
	//최소공배수
	int lcm(int a,int b){
		//return (int) ((a*(long)b)/gcd(a,b));
		return ((a*b)/gcd(a,b));
		//return (a*(b/gcd(a,b)));
		
	}
	public static void main(String[] args){
		LcmGcd lg =new LcmGcd();
		Scanner sc = new Scanner(System.in);
		int input1=0;
		int input2=0;
		
		input1=sc.nextInt();
		input2=sc.nextInt();
		
		System.out.println(lg.lcm(input1, input2));
	}
	
}
