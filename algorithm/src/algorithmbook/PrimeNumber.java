package algorithmbook;

import java.util.Scanner;

public class PrimeNumber {
	public boolean isPrimeNumber(int input){
		if(input==1){
			return false;
		}else if(input==2){
			return true;
		}else{
			for(int i=2; i<input; i++){
				if(input%i == 0){
					return false;
				}
			}
		}
		return true;
	}
	public static void main(String[] args){
		PrimeNumber pn=new PrimeNumber();
		Scanner sc = new Scanner(System.in);
		int input=0;
		input=sc.nextInt();
		System.out.println(pn.isPrimeNumber(input));
	}

}
