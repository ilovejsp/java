package dovelet;

import java.util.Scanner;

public class GcdLcm {

	static int gcd(int a,int b){
		if(a%b ==0 ){
			return b;
		}else{
			return gcd(b, a%b);
		}
	}
	
	public static void main(String[] args) {
		int input1=0;
		int input2=0;
		Scanner sc = new Scanner(System.in);
		int gcd=0;
		int lcm=0;
		
		input1=sc.nextInt();
		input2=sc.nextInt();
		if(input1>=input2){
			gcd=gcd(input1,input2);
		}else{
			gcd=gcd(input2,input1);
			
		}
		lcm=input1*input2/gcd;
		
		System.out.println(gcd+" "+lcm);
		
		
	}

}
