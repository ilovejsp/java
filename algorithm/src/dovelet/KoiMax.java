package dovelet;

import java.util.Scanner;

public class KoiMax {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int input[][]=new int[9][9];
		int max=0;
		int maxI=0;
		int maxJ=0;
		
		for(int i=0; i<input.length; i++){
			for(int j=0; j<input[0].length;j++){
				input[i][j]=sc.nextInt();
				if(input[i][j]>=max){
					max=input[i][j];
					maxI=i+1;
					maxJ=j+1;
					
				}
			}
		}
		System.out.println(max+"\n"+maxI+" "+maxJ);
		
		
	}

}
