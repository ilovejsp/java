package dovelet;

import java.util.Scanner;

public class KoiMystery {

	public static void main(String[] args) {
		int size = 0;
		Scanner sc = new Scanner(System.in);

		size = sc.nextInt();
		int arr[][] = new int[size][2];
		for (int i = 0; i < size; i++) {
			arr[i][0] = sc.nextInt();
			arr[i][1] = 0;
		}
		
		System.out.println(size);
		System.out.print(arr[0][0]+" ");
		arr[0][1]=1;

		for (int i = 0; i < size; i++) {
			
			int point = i + arr[i][0];
			point = point % size;
			while (arr[point][1] != 1) {
				point++;
				point = point % size;
			}
			System.out.print(arr[point][0]+" ");
			arr[point][1] = 1;
		}
	}

}
