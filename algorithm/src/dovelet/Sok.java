package dovelet;

import java.util.Scanner;

public class Sok {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		double input[] =new double[6];
		double prop=Double.MAX_VALUE;
		
		for(int i=0; i<input.length; i++){
			input[i]=sc.nextDouble();
		}
	
		for(int i=0; i<3; i++){
			double tempProp=input[i]/input[i+3];
			if(prop>tempProp){
				prop=tempProp;
			}
		}

		for(int i=0; i<3; i++){
			System.out.println(input[i]-input[i+3]*prop);
		}
		
	}

}
