package dovelet;

import java.util.Scanner;

public class KoiToilet {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		double input=0;
		input=sc.nextDouble();
		double max=0.0;
		double min=0.0;
		
		if(input%2==0){
			max=input/2;
		}else{
			max=Math.round((input/2)+(0.5));
		}
		if(input%3==0){
			min=input/3;
		}else{
			min=Math.round((input/3)+(0.5));
		}
		
		System.out.println((int)max+" "+(int)min);
	}

}
