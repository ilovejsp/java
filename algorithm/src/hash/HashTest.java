package hash;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.omg.CORBA.Current;

public class HashTest {
	static int hash(String s){
		int sum=0;
		
		for(int i=0 ;i<s.length(); i++){
			sum+=(int)s.charAt(i);
		}
		return sum;
	}

	public static void main(String[] args) throws InterruptedException {
		SimpleDateFormat sd = new SimpleDateFormat("yyyymmddhhmmss");
		while(true){
			String str= sd.format(new Date());
			System.out.println(str);
			System.out.println(hash(str));
			Thread.sleep(1000);
		}

	}

}
