package hash;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.DEFAULT;

/**
 * 
 * @author ilovejsps
 * 체인화를 이용한 해시 표
 */
public class HashC {
	/**
	 * 연결리스트의 셀
	 */
	private class Cell{
		MyKey key;
		Object data;
		Cell next;
		
		/**
		 * 셀을 생성한다
		 * @param aKey 키
		 * @param aData 데이터
		 */
		private Cell(MyKey aKey,Object aData){
			key=aKey;
			data=aData;
		}
	}
	
	Cell[] table;
	int bucketSize;
	int nElements;
	
	
	//해시 표의 크기
	static final int DEFAULT_BUCKET_SIZE=50;
	
	/**
	 * 해시 표를 생성한다
	 */
	public HashC(){
		this(DEFAULT_BUCKET_SIZE);
	}
	/**
	 * 해시표를 생성한다
	 * @param size
	 */
	public HashC(int size){
		table = new Cell[size];
		this.bucketSize=size;
		nElements=0;
	}
	/**
	 * 버킷의 개수로 나눈 나머지를 변환
	 * @param key
	 * @return
	 */
	private int hash(MyKey key){
		return key.hashCode() % bucketSize;
	}

	public static void main(String[] args) {

	}

}
