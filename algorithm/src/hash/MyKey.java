package hash;

/**
 * 
 * @author ilovejsps
 * 해시표에서 사용하는 키
 */
public class MyKey {
	String str;
	
	
	/**
	 * 키를 생성한다
	 */
	public MyKey(String s){
		str=s;
	}
	
	/**
	 * 
	 * @param x 비교할키
	 * @return 이키와 키 x가 같다면 true,같지 않다면 false
	 */
	public boolean equals(MyKey x){
		return str.equals(x.str);
	}
	/**
	 * 키의 해시값을 반환
	 */
	public int hashCode(){
		int sum = 0;
		for(int i=0; i<str.length(); i++){
			sum+=(int)str.charAt(i);
		}
		return sum;
	}
	/**
	 * 키를 문자열 형태로 반환
	 */
	public String toString(){
		return str;
	}

}
