package list;

public class MyLinkedList {
	Cell header;
	
	MyLinkedList(){
		header=new Cell("List ���");
	}
	public void insert(int num){
		Cell p = header.next;
		Cell q = header;
		//while(p != null && num > ((Integer)p.data).intValue()){
		while(p != null){
			q=p;
			p=p.next;
		}
		Cell newCell = new Cell(new Integer(num));
		newCell.next = p;
		q.next = newCell;
	}
	/*
	public MyLinkedListIterator iterator(){
		return new MyLinkedListIterator(this);
	}
	*/
	public String toString(){
		String s="";
		s="[";
		for(Cell p=header.next; p!=null ; p=p.next){
			s+=p.data+" ";
		}
		s+="]";
		return s;
	}

	public static void main(String[] args) {
		MyLinkedList list = new MyLinkedList();
		
		System.out.println(list);
		list.insert(3);
		System.out.println(list);
		list.insert(11);
		System.out.println(list);
		list.insert(1);
		System.out.println(list);
		list.insert(100);
		System.out.println(list);
		
	}

}
