package tree;

class BinaryTreeNode {
	String label;
	BinaryTreeNode left;
	BinaryTreeNode right;
	
	BinaryTreeNode(String aLabel,BinaryTreeNode leftTree,BinaryTreeNode rightTree){
		label=aLabel;
		left=leftTree;
		right=rightTree;
	}
	static void preorder(BinaryTreeNode p){
		if(p==null){
			return;
		}
		System.out.println("노드 " +p.label+"을 방문하였다.");
		preorder(p.left);
		preorder(p.right);
	}
	static void inorder(BinaryTreeNode p){
		if(p==null){
			return;
		}
		preorder(p.left);
		System.out.println("노드 " +p.label+"을 방문하였다.");
		preorder(p.right);
	}
	static void postorder(BinaryTreeNode p){
		if(p==null){
			return;
		}
		preorder(p.left);
		preorder(p.right);
		System.out.println("노드 " +p.label+"을 방문하였다.");
	}
	public static void main(String[] args){
		BinaryTreeNode tree = new BinaryTreeNode("a", new BinaryTreeNode("b", null, null), new BinaryTreeNode("c", null, null));
		System.out.println("전위 순회");
		tree.preorder(tree);
		System.out.println("중위 순회");
		tree.inorder(tree);
		System.out.println("후위 순회");
		tree.postorder(tree);
		
	}
	

}
